using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route
{
    public string IATAorOACI { get; set; }
    public int AirlineId { get; set; }
    public string SourceAirportCode { get; set; }
    public int SourceAirportId { get; set; }
    public string DestinationAirportCode { get; set; }
    public int DestinationAirportId { get; set; }
    public string CodeShare { get; set; }
    public int Stops { get; set; }
    public string Equipment { get; set; }

    public Route(string iataOrOaci, int airlineId, string sourceAirportCode, int sourceAirportId, string destinationAirportCode, int destinationAirportId, string codeshare, int stops, string equipment)
    {
        IATAorOACI = iataOrOaci;
        AirlineId = airlineId;
        SourceAirportCode = sourceAirportCode;
        SourceAirportId = sourceAirportId;
        DestinationAirportCode = destinationAirportCode;
        DestinationAirportId = destinationAirportId;
        CodeShare = codeshare;
        Stops = stops;
        Equipment = equipment;
    }
}
