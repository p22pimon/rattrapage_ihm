using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirportIcone : MonoBehaviour
{
    private int airportID;

    private SpriteRenderer sprite;
    private Color initColor;

    private DisplayData displayData;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        initColor = sprite.color;

        displayData = FindObjectOfType<DisplayData>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(transform.parent);
    }

    void OnMouseDown()
    {
        Debug.Log("Airport ID:" + airportID + " clicked!");

        if (displayData != null)
        {
            displayData.HandleAirportClick(airportID);
        }
    }

    void OnMouseOver()
    {
        sprite.color = Color.yellow;
    }

    void OnMouseExit()
    {
        sprite.color = initColor;
    }

    public void SetID(int id)
    {
        airportID = id;
    }
}
