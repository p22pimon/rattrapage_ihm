using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDepth : MonoBehaviour
{
    public float translationSpeed = 5.0f;
    public float minDepth = 0.5f;
    public float maxDepth = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            float deltaDepth = Input.GetAxis("Mouse ScrollWheel") * translationSpeed * 0.001f / Time.deltaTime;
            if (transform.localPosition.z + deltaDepth > -maxDepth &&
                transform.localPosition.z + deltaDepth < -minDepth)
            {
                transform.Translate(new Vector3(0, 0, deltaDepth));
            }
        }
    }

}
