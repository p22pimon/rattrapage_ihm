using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airport
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
    public string IATA { get; set; }
    public string OACI { get; set; }
    public float Latitude { get; set; }
    public float Longitude { get; set; }
    public float Altitude { get; set; }
    public float TimeDifference { get; set; }
    public string TimeSummer { get; set; }
    public string TimeZone { get; set; }

    // Ajoutez d'autres propriétés au besoin, telles que la ville, le fuseau horaire, etc.

    public Airport(int id, string city, string name, string country, string iata, string oaci, float latitude, float longitude, float altitude, float timeDifference, string timeSummer, string timeZone)
    {
        ID = id;
        Name = name;
        City = city;
        Country = country;
        IATA = iata;
        OACI = oaci;
        Latitude = latitude;
        Longitude = longitude;
        Altitude = altitude;
        TimeDifference = timeDifference;
        TimeSummer = timeSummer;
        TimeZone = timeZone;
    }
}
