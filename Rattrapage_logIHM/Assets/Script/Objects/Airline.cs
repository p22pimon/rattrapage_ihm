using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airline
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Alias { get; set; }
    public string IATA { get; set; }
    public string OACI { get; set; }
    public string AreaCode { get; set; }
    public string Country { get; set; }
    public bool IsActive { get; set; }

    public Airline(int id, string name, string alias, string iata, string oaci, string areaCode, string country, bool isActive)
    {
        Id = id;
        Name = name;
        Alias = alias;
        IATA = iata;
        OACI = oaci;
        AreaCode = areaCode;
        Country = country;
        IsActive = isActive;
    }
}
