using System;
using UnityEngine;
using System.Collections.Generic;

public class DisplayData : MonoBehaviour
{
    public float textureLatOffset = 0.0f;
    public float textureLonOffset = 183.0f;

    public float lineResolutionValue = 0.01f;
    public float lineAltitudeFactor = 0.6f;

    public GameObject AirportIconePrefab;
    public GameObject SelectedAirportIconePrefab;

    private Airport selectedAirport;
    private List<GameObject> displayedFlights = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        DataLoader.Instance.LoadAirports("Assets/Data/airports.dat");
        DataLoader.Instance.LoadAirlines("Assets/Data/airlines.dat");
        DataLoader.Instance.LoadRoutes("Assets/Data/routes.dat");

        DisplayAirportsWithMinFlights(5);
    }

    // Update is called once per frame
    void Update()
    {
        // Vérifiez si l'utilisateur a cliqué avec le bouton gauche de la souris
        if (Input.GetMouseButtonDown(0))
        {
            // Lancez un rayon depuis la position de la souris
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // Vérifiez si le rayon a frappé quelque chose
            if (Physics.Raycast(ray, out hit))
            {
                // Vérifiez si ce sur quoi le rayon a frappé n'est pas un AirportIcone
                AirportIcone airportIcone = hit.collider.GetComponent<AirportIcone>();
                if (airportIcone == null)
                {
                    // Effacez tous les vols affichés
                    ClearDisplayedFlights();
                }
            }
        }
    }

    // Display an airport on the map according to its latitude and longitude
    private void displayAirport(int id, float lat, float lon)
    {
        if (AirportIconePrefab != null)
        {
            GameObject newObj = Instantiate(AirportIconePrefab);
            newObj.transform.parent = transform;
            newObj.transform.localPosition = GeoCoordTo3dCoord(lat, lon);

            AirportIcone icone = newObj.GetComponent<AirportIcone>();
            icone.SetID(id);
        }
    }

    // Display a route between two 3D positions on the map
    private void displayRoute(Vector3 pos1, Vector3 pos2, Color color)
    {
        Vector3 v1to2, current_pos;
        Vector3[] vertices;
        float dist, step_dist, current_dist, para_coef_a, para_coef_c, inc_coef = 1;
        int nb_step;

        // Get the distance for pos1 to pos2 and compute the number of steps
        dist = MathF.Sqrt(MathF.Pow(pos2.x - pos1.x, 2)
                 + MathF.Pow(pos2.y - pos1.y, 2)
                 + MathF.Pow(pos2.z - pos1.z, 2));
        nb_step = (int)(dist / lineResolutionValue);
        vertices = new Vector3[(nb_step + 1)];

        // Compute the displacement vector for each step
        v1to2 = (pos2 - pos1) / nb_step;
        step_dist = MathF.Sqrt(MathF.Pow(v1to2.x, 2)
                     + MathF.Pow(v1to2.y, 2)
                     + MathF.Pow(v1to2.z, 2));

        // Fill the vertices tab with the point of the line
        vertices[0] = pos1;
        vertices[nb_step] = pos2;
        current_pos = pos1;
        // Centered the parabole on the middle of the trajectory
        current_dist = -dist / 2.0f;
        // Compute the parabole parameters
        para_coef_c = lineAltitudeFactor * dist;
        para_coef_a = -para_coef_c / MathF.Pow(dist / 2.0f, 2);

        for (int i = 1; i < nb_step; i++)
        {
            current_pos += v1to2;
            current_dist += step_dist;

            inc_coef = 1 + (para_coef_a * MathF.Pow(current_dist, 2) + para_coef_c);

            Vector3 new_pos_n = current_pos * inc_coef;
            vertices[i] = new_pos_n;
        }

        GameObject myLine = new GameObject("Route");
        myLine.transform.parent = transform;
        LineRenderer lr = myLine.AddComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.005f;
        lr.endWidth = 0.005f;
        lr.sortingOrder = 1;

        lr.positionCount = nb_step + 1;
        lr.SetPositions(vertices);

        displayedFlights.Add(myLine);
    }

    // Convert latitude and longitude in 3D coordinates
    private Vector3 GeoCoordTo3dCoord(float lat, float lon)
    {
        float lat_cor = lat + textureLatOffset;
        float lon_cor = lon + textureLonOffset;
        return new Vector3(-MathF.Sin(ToRadians(lon_cor))
                           * MathF.Cos(ToRadians(lat_cor)),
                             MathF.Sin(ToRadians(lat_cor)),
                             MathF.Cos(ToRadians(lon_cor))
                           * MathF.Cos(ToRadians(lat_cor)));
    }

    // Convert angle from degrees to radians
    public static float ToRadians(float angleInDegree)
    {
        return (angleInDegree * MathF.PI) / 180;
    }

    // Affiche les aéroports ayant plus de minFlights vols au départ
    private void DisplayAirportsWithMinFlights(int minFlights)
    {
        foreach (Airport airport in DataLoader.Instance.airports)
        {
            int numberOfFlights = DataLoader.Instance.GetNumberOfFlightsFromAirport(airport.ID);
            if (numberOfFlights > minFlights)
            {
                displayAirport(airport.ID, airport.Latitude, airport.Longitude);
            }
        }
    }

    // Méthode pour afficher les vols réguliers à partir d'un aéroport
    private void DisplayRegularFlightsFromAirport(int airportId)
    {
        // Utilisez les données de DataLoader pour obtenir les vols réguliers de l'aéroport
        List<Route> regularFlights = DataLoader.Instance.GetFlightsFromAirport(airportId);

        // Affichez les vols réguliers sur la carte
        foreach (Route flight in regularFlights)
        {
            displayRoute(GeoCoordTo3dCoord(DataLoader.Instance.GetAirportCoordinatesById(flight.SourceAirportId)[0], DataLoader.Instance.GetAirportCoordinatesById(flight.SourceAirportId)[1]), GeoCoordTo3dCoord(DataLoader.Instance.GetAirportCoordinatesById(flight.DestinationAirportId)[0], DataLoader.Instance.GetAirportCoordinatesById(flight.DestinationAirportId)[1]), Color.green);
        }
    }

    // Méthode pour gérer le clic d'aéroport depuis AirportIcone
    public void HandleAirportClick(int airportID)
    {
        Airport clickedAirport = GetAirportByID(airportID);

        if (clickedAirport != null)
        {
            if (selectedAirport != null && selectedAirport != clickedAirport)
            {
                // Si un autre aéroport était déjà sélectionné, effacer les vols précédemment affichés
                ClearDisplayedFlights();
            }

            // Afficher les vols réguliers à partir de l'aéroport cliqué
            DisplayRegularFlightsFromAirport(clickedAirport.ID);
            selectedAirport = clickedAirport;
        }
    }

    // Méthode pour obtenir un aéroport à partir de son ID
    private Airport GetAirportByID(int id)
    {
        foreach (Airport airport in DataLoader.Instance.airports)
        {
            if (airport.ID == id)
            {
                return airport;
            }
        }
        return null;
    }

    private void ClearDisplayedFlights()
    {
        foreach (GameObject flightObject in displayedFlights)
        {
            // Détruisez l'objet représentant le vol
            Destroy(flightObject);
        }

        // Effacez la liste des vols affichés
        displayedFlights.Clear();
    }
}
