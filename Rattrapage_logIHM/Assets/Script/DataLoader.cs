using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System.Globalization;

public class DataLoader : MonoBehaviour
{

    private static DataLoader instance;

    public static DataLoader Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<DataLoader>();
            }
            return instance;
        }
    }
    
    public List<Airport> airports = new List<Airport>();
    public List<Airline> airlines = new List<Airline>();
    public List<Route> routes = new List<Route>();

    private Dictionary<int, int> flightsCountByAirport = new Dictionary<int, int>();
    private Dictionary<int, List<Route>> flightsByAirport = new Dictionary<int, List<Route>>();


    // Start is called before the first frame update
    void Start()
    {

    }

    internal void LoadAirports(string filePath)
    {
        try
        {
            using (StreamReader sr = new StreamReader(filePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] array = line.Split(',');

                    int id;
                    if (int.TryParse(array[0], out id))
                    {
                        string name = array[1];
                        string city = array[2];
                        string country = array[3];
                        string iata = array[4];
                        string oaci = array[5];

                        CultureInfo culture = CultureInfo.InvariantCulture;

                        float latitude, longitude;
                        if (float.TryParse(array[6], NumberStyles.Float, culture, out latitude) && float.TryParse(array[7], NumberStyles.Float, culture, out longitude))
                        {
                            float altitude, timeDifference;
                            if (float.TryParse(array[8], NumberStyles.Float, culture, out altitude) && float.TryParse(array[9], NumberStyles.Float, culture, out timeDifference))
                            {
                                string timeSummer = array[10];
                                string timeZone = array[11];

                                // Créer une instance de la classe Airport
                                Airport airport = new Airport(id, name, city, country, iata, oaci, latitude, longitude, altitude, timeDifference, timeSummer, timeZone);

                                // Ajouter l'aéroport à la liste
                                airports.Add(airport);
                            }
                            else
                            {
                                Debug.LogError($"Failed to parse altitude or timeDifference for airport ID {id}. Altitude: {array[8]}, TimeDifference: {array[9]}");
                            }
                        }
                        else
                        {
                            Debug.LogError($"Failed to parse latitude or longitude for airport ID {id}. Latitude: {array[6]}, Longitude: {array[7]}");
                        }
                    }
                    else
                    {
                        Debug.LogError($"Failed to parse latitude or longitude for airport ID {id}.");
                    }
                }
            }   
        }
        catch (Exception e)
        {
            Debug.Log("Erreur lors du chargement des aéroports :");
            Debug.Log(e.Message);
        }
    }

    internal void LoadAirlines(string filePath)
    {
        try
        {
            using (StreamReader sr = new StreamReader("Assets/Data/airlines.dat"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] array = line.Split(",");
                    
                    int id = int.Parse(array[0]);
                    string name = array[1];
                    string alias = array[2];
                    string iata = array[3];
                    string oaci = array[4];
                    string areaCode = array[5];
                    string country = array[6];
                    bool isActive;
                    if (array[7] == "Y")
                    {
                        isActive = true;
                    }
                    else
                    {
                        isActive = false;
                    }
                
                Airline airline = new Airline(id, name, alias, iata, oaci, areaCode, country, isActive);
                
                airlines.Add(airline);
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Erreur lors du chargement des compagnies aériennes :");
            Debug.Log(e.Message);
        }
    }

    internal void LoadRoutes(string filePath)
    {
        try
        {
            using (StreamReader sr = new StreamReader("Assets/Data/routes.dat"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {

                    CultureInfo culture = CultureInfo.InvariantCulture;

                    string[] array = line.Split(",");

                    if (Array.IndexOf(array, "\\N") != -1)
                    {
                        continue;
                    }
                    else
                    {
                        string iataOrOaci = array[0];
                        int airlineId = int.Parse(array[1]);
                        string sourceAirportCode = array[2];
                        string destinationAirportCode = array[4];
                        string codeShare = array[6];
                        string equipment = array[8];

                        int sourceAirportId, destinationAirportId, stops;

                        if (int.TryParse(array[3], out sourceAirportId) && int.TryParse(array[5], out destinationAirportId) && int.TryParse(array[7], out stops))
                        {
                            Route route = new Route(iataOrOaci, airlineId, sourceAirportCode, sourceAirportId, destinationAirportCode, destinationAirportId, codeShare, stops, equipment);

                            routes.Add(route);

                            // Mettez à jour le dictionnaire du nombre de vols par aéroport
                            if (flightsCountByAirport.ContainsKey(sourceAirportId))
                            {
                                flightsCountByAirport[sourceAirportId]++;
                            }                      
                            else
                            {
                                flightsCountByAirport[sourceAirportId] = 1;
                            }

                            // Mettez à jour le dictionnaire de la liste des vols par aéroport
                            if (flightsByAirport.ContainsKey(sourceAirportId))
                            {
                                flightsByAirport[sourceAirportId].Add(route);
                            }   
                            else
                            {
                                flightsByAirport[sourceAirportId] = new List<Route> { route };
                            }
                        }
                        else
                        {
                            Debug.LogError($"Erreur lors du chargement des routes. Ligne : {line}");
                        }
                    }
                            
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("Erreur lors du chargement des routes :");
            Debug.Log(e.Message);
        }
    }

    public int GetNumberOfFlightsFromAirport(int airportId)
    {
        if (flightsCountByAirport.ContainsKey(airportId))
        {
            int numberOfFlights = flightsCountByAirport[airportId];
            return numberOfFlights;
        }
        else
        {
            return 0;
        }
    }

    public List<Route> GetFlightsFromAirport(int airportId)
    {
        if (flightsByAirport.ContainsKey(airportId))
        {
            List<Route> flights = flightsByAirport[airportId];
            return flights;
        }
        else
        {
            Debug.Log($"Airport with code {airportId} not found.");
            return new List<Route>();
        }
    }

    public Vector2 GetAirportCoordinatesById(int airportId)
    {
        foreach (var airport in airports)
        {
            if (airport.ID == airportId)
            {
                return new Vector2(airport.Latitude, airport.Longitude);
            }
        }

        // Retourner un vecteur nul si l'aéroport avec cet ID n'est pas trouvé
        return Vector2.zero;
    }


    // Update is called once per frame
    void Update()
    {
        
    }

}
